
Path: /etc/ImageMagick-6/type-ghostscript.xml
  Font: AvantGarde-Book
    family: AvantGarde
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/a010013l.pfb
  Font: AvantGarde-BookOblique
    family: AvantGarde
    style: Oblique
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/a010033l.pfb
  Font: AvantGarde-Demi
    family: AvantGarde
    style: Normal
    stretch: Normal
    weight: 600
    glyphs: /usr/share/fonts/type1/gsfonts/a010015l.pfb
  Font: AvantGarde-DemiOblique
    family: AvantGarde
    style: Oblique
    stretch: Normal
    weight: 600
    glyphs: /usr/share/fonts/type1/gsfonts/a010035l.pfb
  Font: Bookman-Demi
    family: Bookman
    style: Normal
    stretch: Normal
    weight: 600
    glyphs: /usr/share/fonts/type1/gsfonts/b018015l.pfb
  Font: Bookman-DemiItalic
    family: Bookman
    style: Italic
    stretch: Normal
    weight: 600
    glyphs: /usr/share/fonts/type1/gsfonts/b018035l.pfb
  Font: Bookman-Light
    family: Bookman
    style: Normal
    stretch: Normal
    weight: 300
    glyphs: /usr/share/fonts/type1/gsfonts/b018012l.pfb
  Font: Bookman-LightItalic
    family: Bookman
    style: Italic
    stretch: Normal
    weight: 300
    glyphs: /usr/share/fonts/type1/gsfonts/b018032l.pfb
  Font: Courier
    family: Courier
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n022003l.pfb
  Font: Courier-Bold
    family: Courier
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n022004l.pfb
  Font: Courier-BoldOblique
    family: Courier
    style: Oblique
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n022024l.pfb
  Font: Courier-Oblique
    family: Courier
    style: Oblique
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n022023l.pfb
  Font: fixed
    family: Helvetica
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n019003l.pfb
  Font: Helvetica
    family: Helvetica
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n019003l.pfb
  Font: Helvetica-Bold
    family: Helvetica
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n019004l.pfb
  Font: Helvetica-BoldOblique
    family: Helvetica
    style: Italic
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n019024l.pfb
  Font: Helvetica-Narrow
    family: Helvetica Narrow
    style: Normal
    stretch: Condensed
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n019043l.pfb
  Font: Helvetica-Narrow-Bold
    family: Helvetica Narrow
    style: Normal
    stretch: Condensed
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n019044l.pfb
  Font: Helvetica-Narrow-BoldOblique
    family: Helvetica Narrow
    style: Oblique
    stretch: Condensed
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n019064l.pfb
  Font: Helvetica-Narrow-Oblique
    family: Helvetica Narrow
    style: Oblique
    stretch: Condensed
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n019063l.pfb
  Font: Helvetica-Oblique
    family: Helvetica
    style: Italic
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n019023l.pfb
  Font: NewCenturySchlbk-Bold
    family: NewCenturySchlbk
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/c059016l.pfb
  Font: NewCenturySchlbk-BoldItalic
    family: NewCenturySchlbk
    style: Italic
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/c059036l.pfb
  Font: NewCenturySchlbk-Italic
    family: NewCenturySchlbk
    style: Italic
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/c059033l.pfb
  Font: NewCenturySchlbk-Roman
    family: NewCenturySchlbk
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/c059013l.pfb
  Font: Palatino-Bold
    family: Palatino
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/p052004l.pfb
  Font: Palatino-BoldItalic
    family: Palatino
    style: Italic
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/p052024l.pfb
  Font: Palatino-Italic
    family: Palatino
    style: Italic
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/p052023l.pfb
  Font: Palatino-Roman
    family: Palatino
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/p052003l.pfb
  Font: Symbol
    family: Symbol
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/s050000l.pfb
  Font: Times-Bold
    family: Times
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n021004l.pfb
  Font: Times-BoldItalic
    family: Times
    style: Italic
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n021024l.pfb
  Font: Times-Italic
    family: Times
    style: Italic
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n021023l.pfb
  Font: Times-Roman
    family: Times
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n021003l.pfb

Path: System Fonts
  Font: Century-Schoolbook-L-Bold
    family: Century Schoolbook L
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/c059016l.pfb
  Font: Century-Schoolbook-L-Bold-Italic
    family: Century Schoolbook L
    style: Italic
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/c059036l.pfb
  Font: Century-Schoolbook-L-Italic
    family: Century Schoolbook L
    style: Italic
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/c059033l.pfb
  Font: Century-Schoolbook-L-Roman
    family: Century Schoolbook L
    style: Normal
    stretch: Normal
    weight: 500
    glyphs: /usr/share/fonts/type1/gsfonts/c059013l.pfb
  Font: DejaVu-Sans
    family: DejaVu Sans
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/truetype/dejavu/DejaVuSans.ttf
  Font: DejaVu-Sans-Bold
    family: DejaVu Sans
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/truetype/ttf-dejavu/DejaVuSans-Bold.ttf
  Font: DejaVu-Sans-Mono
    family: DejaVu Sans Mono
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/truetype/ttf-dejavu/DejaVuSansMono.ttf
  Font: DejaVu-Sans-Mono-Bold
    family: DejaVu Sans Mono
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/truetype/dejavu/DejaVuSansMono-Bold.ttf
  Font: DejaVu-Serif
    family: DejaVu Serif
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/truetype/dejavu/DejaVuSerif.ttf
  Font: DejaVu-Serif-Bold
    family: DejaVu Serif
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/truetype/ttf-dejavu/DejaVuSerif-Bold.ttf
  Font: Dingbats
    family: Dingbats
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/d050000l.pfb
  Font: Droid-Sans-Fallback
    family: Droid Sans Fallback
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/truetype/droid/DroidSansFallbackFull.ttf
  Font: Free-3-of-9-Regular
    family: Free 3 of 9
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/truetype/free3of9/free3of9.ttf
  Font: Nimbus-Mono-L
    family: Nimbus Mono L
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n022003l.pfb
  Font: Nimbus-Mono-L-Bold
    family: Nimbus Mono L
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n022004l.pfb
  Font: Nimbus-Mono-L-Bold-Oblique
    family: Nimbus Mono L
    style: Oblique
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n022024l.pfb
  Font: Nimbus-Mono-L-Regular-Oblique
    family: Nimbus Mono L
    style: Oblique
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n022023l.pfb
  Font: Nimbus-Roman-No9-L
    family: Nimbus Roman No9 L
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n021003l.pfb
  Font: Nimbus-Roman-No9-L-Medium
    family: Nimbus Roman No9 L
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n021004l.pfb
  Font: Nimbus-Roman-No9-L-Medium-Italic
    family: Nimbus Roman No9 L
    style: Italic
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n021024l.pfb
  Font: Nimbus-Roman-No9-L-Regular-Italic
    family: Nimbus Roman No9 L
    style: Italic
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n021023l.pfb
  Font: Nimbus-Sans-L
    family: Nimbus Sans L
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n019003l.pfb
  Font: Nimbus-Sans-L-Bold
    family: Nimbus Sans L
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n019004l.pfb
  Font: Nimbus-Sans-L-Bold-Condensed
    family: Nimbus Sans L
    style: Normal
    stretch: Condensed
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n019044l.pfb
  Font: Nimbus-Sans-L-Bold-Condensed-Italic
    family: Nimbus Sans L
    style: Italic
    stretch: Condensed
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n019064l.pfb
  Font: Nimbus-Sans-L-Bold-Italic
    family: Nimbus Sans L
    style: Italic
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/n019024l.pfb
  Font: Nimbus-Sans-L-Regular-Condensed
    family: Nimbus Sans L
    style: Normal
    stretch: Condensed
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n019043l.pfb
  Font: Nimbus-Sans-L-Regular-Condensed-Italic
    family: Nimbus Sans L
    style: Italic
    stretch: Condensed
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n019063l.pfb
  Font: Nimbus-Sans-L-Regular-Italic
    family: Nimbus Sans L
    style: Italic
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/n019023l.pfb
  Font: Noto-Mono
    family: Noto Mono
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/truetype/noto/NotoMono-Regular.ttf
  Font: Silent-Reaction
    family: Silent Reaction
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/truetype/silentreaction/Silent Reaction.ttf
  Font: Standard-Symbols-L
    family: Standard Symbols L
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/s050000l.pfb
  Font: Sunshine-In-My-Soul
    family: Sunshine In My Soul
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/truetype/sunshineinmysoul/Sunshine in my Soul.ttf
  Font: URW-Bookman-L-Demi-Bold
    family: URW Bookman L
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/b018015l.pfb
  Font: URW-Bookman-L-Demi-Bold-Italic
    family: URW Bookman L
    style: Italic
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/b018035l.pfb
  Font: URW-Bookman-L-Light
    family: URW Bookman L
    style: Normal
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/b018012l.pfb
  Font: URW-Bookman-L-Light-Italic
    family: URW Bookman L
    style: Italic
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/b018032l.pfb
  Font: URW-Chancery-L-Medium-Italic
    family: URW Chancery L
    style: Italic
    stretch: Normal
    weight: 500
    glyphs: /usr/share/fonts/type1/gsfonts/z003034l.pfb
  Font: URW-Gothic-L-Book
    family: URW Gothic L
    style: Normal
    stretch: Normal
    weight: 300
    glyphs: /usr/share/fonts/type1/gsfonts/a010013l.pfb
  Font: URW-Gothic-L-Book-Oblique
    family: URW Gothic L
    style: Oblique
    stretch: Normal
    weight: 300
    glyphs: /usr/share/fonts/type1/gsfonts/a010033l.pfb
  Font: URW-Gothic-L-Demi
    family: URW Gothic L
    style: Normal
    stretch: Normal
    weight: 600
    glyphs: /usr/share/fonts/type1/gsfonts/a010015l.pfb
  Font: URW-Gothic-L-Demi-Oblique
    family: URW Gothic L
    style: Oblique
    stretch: Normal
    weight: 600
    glyphs: /usr/share/fonts/type1/gsfonts/a010035l.pfb
  Font: URW-Palladio-L-Bold
    family: URW Palladio L
    style: Normal
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/p052004l.pfb
  Font: URW-Palladio-L-Bold-Italic
    family: URW Palladio L
    style: Italic
    stretch: Normal
    weight: 700
    glyphs: /usr/share/fonts/type1/gsfonts/p052024l.pfb
  Font: URW-Palladio-L-Italic
    family: URW Palladio L
    style: Italic
    stretch: Normal
    weight: 400
    glyphs: /usr/share/fonts/type1/gsfonts/p052023l.pfb
  Font: URW-Palladio-L-Roman
    family: URW Palladio L
    style: Normal
    stretch: Normal
    weight: 500
    glyphs: /usr/share/fonts/type1/gsfonts/p052003l.pfb
